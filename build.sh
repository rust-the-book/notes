#!/bin/bash

mdbook build

if [[ "$?" != "0" ]]
then
   exit 1
fi

cp -r book/html/* public
