# Bringing paths into scope with use keyword

```{.rust}
mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}
    }
}

use crate::front_of_house::hosting;

pub fn eat_at_restaurant() {
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
}
```

- `use` and `path` - similar to `symlink` in `filesystem`
- `hosting` valid to be used alone!
- `privacy` of paths also checked!
- can also have `relative` path like `use front_of_house::hosting`

## Creating Idiomatic use Paths

```{.rust}
use crate::front_of_house::hosting::add_to_waitlist;

pub fn eat_at_restaurant() {
    add_to_waitlist();
}
```

- we could do this, but not *idiomatic*!
- `hosting::add_to_waitlist()` says, `add_to_waitlist` isn't *locally defined*

```{.rust}
use std::collections::HashMap;

use std::io;
use std::fmt;

fn do_something_io() -> io::Result<()> {}

fn do_something_fmt() -> fmt::Result {}
```

- its usually ok to bring in `struct`, `enum` and like directly into scope
- but caveat is when two modules export same type like `io::Result` and `fmt::Result`

## Providing new name with as keyword

```{.rust}
use std::io::Result as IOResult;
use std::fmt::Result;

fn do_something_io() -> IOResult<()> {}

fn do_something_fmt() -> Result {}
```

- `as` differentiates `io::Result` and `fmt::Result`

## Re-exporting names with pub use

```{.rust}
mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}
    }
}

pub use crate::front_of_house::hosting;

pub fn eat_at_restaurant() {
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
}
```

- `hosting` name is private within the lib
- using `pub` allows *external code* to use it
- good way to maintain one structure internally, but expose another for external code to use
- like customers wont understand *front of house* & *back of house*

## Use nested paths to clean up large use list

```{.rust}
use std::{io, Cmp::Ordering};
use std::io::{self, Write};
```

- above prevents multiple `use` statements from same `std` module
- `self` references `io` module & also brings in `Write`

## Glob operator

```{.rust}
use std::collections::*;
```

- brings all `public` items to scope
- but obscures which items comes from where!
- use in tests and will be explored in that chapter
