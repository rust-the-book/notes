# Separating modules into different files

```{.rust}
mod front_of_house;

pub use crate::front_of_house::hosting;

pub fn eat_at_restaurant() {
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
}
```

- shows changes to `crate root` - `src/lib.rs`
- move `front_of_house` module code into `src/front_of_house.rs`

```{.rust}
pub mod hosting;
```

- shows changes to `src/front_of_house.rs`
- move `hosting` module code into `src/front_of_house/hosting.rs`

```{.rust}
pub fn add_to_waitlist() {}
```

- shows changes to `src/front_of_house/hosting.rs
