# Packages and Crates

- `crate` - `library` or `binary`
- `package` - one or more `crates` provide set of functionalities
- `crate root` - file rust designates as `root` of module system

```{.rust}
cargo new my-project
```

- `cargo` creates new `package`
- has `Cargo.toml`
- `src/main.rs` - `crate root` of `binary crate`
- `src/lib.rs` - `crate root` of `library crate`
- `name` of the `crate` is *same* as `package name`
- `package` can have *both* `binary` & `library` crates
- `package` can have *multiple* `binary crate` placed with `src/bin` directory
- `crate` group *related functionality* together in `scope`
