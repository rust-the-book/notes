# Paths for Referring to an Item in the Module Tree

- `path` to navigate `module tree` (similar to *filesystem*)
- `absolute path` - from `crate root` using *crate's name* or literal `crate`
- `relative path` - from *curent module* using `self` or `super`
- path separated by `::`

```{.rust}
mod front_of_house {
    mod hosting {
        fn add_to_waitlist() {} 
    }
}

pub fn eat_at_restaurant() {
    // abs path
    crate::front_of_house::hosting::add_to_waitlist();

    // relative path
    front_of_house::hosting::add_to_waitlist();
}
```

- similar to navigating a file in filesystem
- to access `passwd` file from within `/etc`
    - absolute `/etc/passwd`
    - relative `./passwd`

```{.rust}
mod customer_experience {
    mod front_of_house {
        mod hosting {
            fn add_to_waitlist() {} 
        }
    }

    pub fn eat_at_restaurant() {
        // abs path modified
        crate::customer_experience::front_of_house::hosting::add_to_waitlist();

        // relative path stays same
        front_of_house::hosting::add_to_waitlist();
    }
}
```
- if both `front_of_house` & `eat_at_restaurant` moved into `customer_experience`
    - `absolute` path need to updated
    - `relative` path stays the same

```{.rust}
mod front_of_house {
    mod hosting {
        fn add_to_waitlist() {} 
    }
}

mod customer_experience {
    pub fn eat_at_restaurant() {
        // abs path stays same
        crate::front_of_house::hosting::add_to_waitlist();

        // relative path modified
    }
}
```
- if only `eat_at_restaurant` moved into `customer_experience`
    - `absolute` path stays the same
    - `relative` path needs to be modified

- in all of the above code, `hosting` is `private` by *default*
- `module` create `privacy boundary`
- anything inside it is `private`
- useful when changing inner working of library, without affecting external code
- expose inner code using `pub` keyword

## Exposing paths with pub keyword

```{.rust}
mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {} 
    }
}

pub fn eat_at_restaurant() {
    // abs path
    crate::front_of_house::hosting::add_to_waitlist();

    // relative path
    front_of_house::hosting::add_to_waitlist();
}
```

- even `child modules` are `private` by default (because, `anything` inside a `module` is `private`)
- `front_of_house` & `eat_at_restuarant` are *siblings* under `crate`, so can access each other
- `hosting` & `add_to_waitlist` marked as `pub`, so *accessible*

## Starting relative path with super

```{.rust}
fn serve_order() {}

mod back_of_house() {
    fn fix_incorrect_order() {
        cook_food();

        super::serve_order();
    }
}
```

- similar to `..` from within a folder
- `super::serve_order` - goto `parent` module of `back_of_house` & access `serve_order`
- as long as, `serve_order` & `back_of_house` are *moved together*, `super` works as is

## Making struct and enums public

```{.rust}
mod back_of_house {
    pub struct Breakfast {
        pub toast: String,
        seasonal_fruit: String;
    }

    impl Breakfast {
        pub fn summer(toast: &str)  -> Breakfast {
            Breakfast {
                toast: String::from(toast),
                seasonal_fruit: String::from("peaches") 
            } 
        }
    }

    pub enum Appetizer {
        Soup,
        Salad
    }
}

pub fn eat_at_restaurant() {
    // order breakfast in summer with Rye toast
    let mut meal = back_of_house::Breakfast::summer("Rye");

    // change bread
    meal.toast = String::from("wheat");
    println!("I'd like {} toast please", meal.toast);

    // following won't compile
    meal.seasonal_fruit = String::from("pineapple");

    let order1 = back_of_house::Appetizer::Soup;
    let order2 = back_of_house::Appetizer::Salad;
}
```

- `Breakfast` has a *private* field `seasonal_field`, so need a *public associated* function `summer` to create instance
- when `enum` marked *pub*, all `variants` are `pub`
