# Defining Modules to control Scope and Privacy

- `Module`
    - *organize* code in `crate`
    - control *privacy* of items (`public` or `private`)

```{.rust}
restaurant library

crate -> crate root
 └── front_of_house
     ├── hosting
     │   ├── add_to_waitlist
     │   └── seat_at_table
     └── serving
         ├── take_order
         ├── serve_order
         └── take_payment
```

- `crate root` - contents of `src/main.rs` or `src/lib.rs`
- form a *implicit* module called `crate` @ root of module structure called `module tree`
