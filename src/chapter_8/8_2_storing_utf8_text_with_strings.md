# Storing UTF-8 encoded text with Strings

## What is a String?

- `str` - *only one* `string` type in *core language*
- usually in `borrowed` form as `&str`
- `String` - in `standard library`, growable, mutable, owned & utf-8 encoded

## Create a new String

```{.rust}
let mut s = String::new();

let data = "initial contents";
let s = data.to_string();
let s = "initial contents".to_string();
let s = String::from("initial contents");
let s = String::from(&s);
let s = String::from(&s[..]);

let hello = String::from("السلام عليكم");
let hello = String::from("Dobrý den");
let hello = String::from("Hello");
let hello = String::from("שָׁלוֹם");
let hello = String::from("नमस्ते");
let hello = String::from("こんにちは");
let hello = String::from("안녕하세요");
let hello = String::from("你好");
let hello = String::from("Olá");
let hello = String::from("Здравствуйте");
let hello = String::from("Hola");
```

- `new` creates an empty `String`
- `to_string` from `Display` trait returns `String`, `&str` implements it
- `from` creates from `&str`
- from `string slices`
- from `String ref`
- `UTF-8` encoded

## Updating a String

- can grow like `Vec`
- can use `+` & `format!` macro to *concatenate*

### Appending to a String with push_str & push

```{.rust}
let mut s = String::from("Hello");
s.push_str(" world");
s.push('!');
```

- `push_str` takes `string slice`, we don't want to take ownership
- `push` takes a `character`

### Concatenation with + & format! macro

```{.rust}
let s1 = String::from("Hello");
let s2 = String::from(", World!");
let s = s1 + &s2;
```

- `s1` is `moved` in `s1 + &s2`
- `+` is implemented like `fn add(self, other: &str) -> String` (illustration, actual function uses `Generics`)
- but `&s2` is `&String`, not `&str`!
- rust uses `deref coercion` to convert `&s2` into `&s2[..]`

```{.rust}
let s1 = String::from("tic");
let s2 = String::from("tac");
let s3 = String::from("toe");
let s = s1 + " " + &s2 + " " + &s3;
let s = format!("{} {} {}", s1, s2, s3);
```

- `format!` similar to `println!`, but returns `String` not to `stdout`
- doesn't take `ownership`
- simple & easy to use

## Indexing into Strings

```{.rust}
let s1 = String::from("hello");
let first = s1[0];
```

- indexing a string - trivial in many languages
- above results in error

### Internal representation

- `String` wrapper over `Vec<u8>`

```{.rust}
let s = String::from("hola");
let len = s.len();
```

- returns `4`, means `4 bytes` long & each char is `1 byte` in `UTF-8`

```{.rust}
let s = String::from("Здравствуйте");
let len = s.len();
```

- actual length is `24`
- each char takes `2 bytes` in `UTF-8`

```{.rust}
let s = String::from("Здравствуйте");
let first = &s[0];
```

- consider this, what is value of `first`? `3`? or its `UTF-8` byte value `208`?
- `208` is a not character & not what user expected!
- even with `ascii` content, `&"hello[0]"`, still has a `byte value`
- rust doesn't like to guess, so all these result in errors

### Bytes, scalar values & graphmeme clusters

```{.rust}
let wikipedia = "விக்கிப்பீடியா"
```

- 3 ways to look at above string
    - *bytes* - `224 174 181 224 174 191 224 174 149 224 175 141 224 174 149 224 174 191 224 174 170 224 175 141 224 174 170 224 175 128 224 174 159 224 174 191 224 174 175 224 174 190 `
    - *scalar values* - `வ ி க ் க ி ப ் ப ீ ட ி ய ா `
    - *graphmeme clusters* - `விக்கிப்பீடியா`
- those punctuation characters are called `diacritics` & make sense only when in a word
- allows developer to use representation thats appropriate
- *indexing* is supposed to be `O(1)`, but here, rust has to go through entire string to make sense of its structure, so its not supported

## Slicing strings

```{.rust}
let hello = "Здравствуйте";

let s = &hello[0..4];
```

- here, each character is two bytes, so we would get `Зд`
- how about `&hello[0..1]`? `panic`! because `slices` should be at `valid character boundaries`

## Iterating over strings

```{.rust}
let wikipedia = "விக்கிப்பீடியா"

for char in wikipedia.chars() {
    println!("{}", char);
}

for byte in wikipedia.bytes() {
    println!("{}", byte);
}
```

- use `chars()` method to access `scalar value`. **note:** this would return punctuations separated!
- use `bytes()` to access `byte value`
- `crates.io` has libraries that specialize in this area!
