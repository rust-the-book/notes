# Storing list of values in vectors

- store *variable* no. of elements of *same type*
- stored in *heap* next to each other

## Creating a vector

```{.rust}
lev v: Vec<i32> = Vec::new();
let v = vec![1, 2, 3];
```

- implemented with `generics`
- `Vec<i32>` says this vec holds items of type `i32`
- without initial values, type must be specified
- `vec!` macro creates `Vec` with three `i32` items

## Updating a vector

```{.rust}
let mut v = Vec::new();

v.push(1);
v.push(2);
v.push(3);
v.push(4);
```

- `push` on `Vec` to insert data
- need to mark as `mut` to update
- when data is first inserted, type is inferred, so no error thrown
- but would be an error if no data inserted

## Dropping a vector, drops its elements

```{.rust}
{
    let v = vec![1, 2, 3];
}
```

- when `v` goes `out of scope`, all elements are `dropped`
- **cavaeat** tricky when elements stored are `references`

## Reading elements of vectors

```{.rust}
let v = vec![1, 2, 3];

let third: &i32 = &v[2];
println!("Third element {}", third);

match v.get(2) {
    Some(third) => println!("Third element {}", third),
    None => println!("No third element")
}
```

- `get` returns `Option<&T>`
- `&[2]` returns `reference` of third element

```{.rust}
let v = vec![1, 2, 3];

let does_not_exist = &v[100];
let does_not_exist = v.get(100);
```

- `&v[100]` causes `panic`, useful when you want to `fail fast`
- `v.get(100)` returns `None`, useful when you want to `fail safe`

```{.rust}
let mut v = vec![1, 2, 3];
let first = &v[0];
v.push(6);
println!("first is {}", first);
```

- can't have `mutable` & `immutable` refs in same scope
- `&v[0]` is `mutabale` ref of first element & later used in print
- `v.push(6)` is `mutable` ref to `v`
- but why? `first` is `immutable` ref to element, not `v`!
- rust might re-allocate elements to another location, if current location doesn't have enough space to place element next to each other
- `first` would be pointing to `deallocated memory`, which is a big no no!

## Iterating over values in vector

```{.rust}
let v = vec![1, 2, 3];

for i in &v {
    println!("i is {}", i);
}

for i in &mut v {
    *i += 1;
}
```

- `&v` iterate `immutable` ref of each element
- `&mut v` iterate `mutable` ref of each element
- `*i` to `deref pointer` to actual value

## Using enum to store multiple types

```{.rust}
enum SpreadsheetCell {
    Int(i32),
    Float(f64),
    Text(String)
}

let row = vec![
    SpreadsheetCell::Int(10),
    SpreadsheetCell::Float(3.14),
    SpreadsheetCell::Text(String::from("PI"))
];
```

- `Vec` stores values of same type
- `enum variants` can be different types, but come under same `enum type`
- like a spreadsheel cell can have different values
- use `enum variants` to store values of different types
- store `enum` inside a `Vec`
