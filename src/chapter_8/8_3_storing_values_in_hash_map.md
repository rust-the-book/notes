# Storing keys with in associated values in hash maps

## Creating a hash map

```{.rust}
use std::collections::HashMap;

let mut scores = HashMap::new();

scores.insert(String::from("Blue"), 10);
scores.insert(String::from("Yellow"), 50);

let teams = vec![String::from("Blue"), String::from("Yellow")];
let initial_scores = vec![10, 50];

let scores: HashMap<_, _> = teams.iter().zip(initial_scores.iter()).collect();
```

- stored on `heap` 
- `keys` & `values` of `same type`
- not included in `prelude`, least often used!
- can also be created using `collect` from vector of tuples, rust needs type annotations, but we use `_` as placeholder, rust can infer type from `vec`

## HashMap and ownership

```{.rust}
let field_name = String::from("key");
let field_value = String::from("value");
let mut hm = HashMap::new();
hm.insert(field_name, field_value);
```

- types with `Copy` trait are copied
- types like `String` are moved like `field_name` & `field_value`
- can store `reference`, but should be valid as long as hashmap!

## Accessing values in HashMap

```{.rust}
let mut scores = HashMap::new();

scores.insert(String::from("Blue"), 10);
scores.insert(String::from("Yellow"), 50);

let score = scores.get("Blue");

for (k, v) in &scores {
    println!("{}: {}", k, v);
}
```

- `get` takes a `&K`, here its `&String`
- returns `Option<&V>`, here its `Option<&String>`
- can iterate with `for loop`

## Updating a HashMap

### Overwriting a value

```{.rust}
let mut scores = HashMap::new();

scores.insert(String::from("Blue"), 10);
scores.insert(String::from("Blue"), 25);

println!("{:?}", scores);
```

- inserting value with same key, replace old value with new one

### Only insert when key has no value

```{.rust}
let mut scores = HashMap::new();

scores.insert(String::from("Blue"), 10);

*scores.entry(String::from("Yellow")).or_insert(50) *= 10; // 500
let mut b = scores.entry(String::from("Blue")).or_insert(40); 
b += 10; // 50
```

- `entry` special api, takes `key` as parameter
- returns `enum` called `Entry`, says a value might or might not exist
- `or_insert`, if not present `insert` & return `mutable ref` of value, else return `mutable ref` of value

### Updating a value based on old value

```{.rust}
let text = "hello world wonderful world";
let mut words = HashMap::new();

for word in text.split_whitespace() {
    let counter = word.entry(word).or_insert(0);
    *counter += 1;
}

println!("{:#?}", words);
```

- `or_insert` returns `&mut V`, here `&mut i32`, stored in `counter`
- `deref` counter with `*`

### Hashing Functions

- `HashMap` uses cryptographiclly strong hash functions to prevent agains `DOS`
- not fastest, trade off for security
- provide own `hasher` that implements `BuildHasher` trait
