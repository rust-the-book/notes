# Functions

```{.rust}
fn main() {
    another_function();
}

fn another_function() {}
```

- *pervasive* in rust
- *main* 1st function
- `fn` keyword to declare
- *snake* case for functions & variables
- order of definition doesn't matter

## Function parameters

```{.rust}
fn main() {
    another_function(5, 10);
}

fn another_function(x: i32, y: i32) {}
```

- *parameters* defined as part of function
- *arguments* concrete values passed to a function
- types must be explicit (so, need not be explicit from calling code)
- multiple params separated by comma

## Function bodies contain statements & expression

```{.rust}
fn main() {
    let x = {
        let y = 10;
        10 + 1
    }
}
```

- has series of *statements*
- optionally end with *expression*
- *expression* can be part of *statement*
- rust is *expression* based language
- *statements*
    - does action & no return value
    - *function definition* is also a *statement* (like `main`)
    - `let x = (let y = 6);` is *invalid*
    - in `C`, `x = y = 6` is valid
- *expression*
    - evaluates to a value
    - `5 + 6` evaluates to `11`
    - calling *function* & *macros*
    - `{}` *scope blocks* are expression

## Function with return values

```{.rust}
fn main() {
    let sum = add(10, 20);
}

fn add(x: i32, y: i32) -> i32 {
    if (x > y) {
        return x + y + 10; 
    }

    x + y
}
```

- return type indicated after `->`
- use `return` for a pre-mature *exodus*
- generally, last *expression* (without semicolon) is implicitly returned
- `x + y` is implicitly returned
- function with *no return type* indicates a unit type `()` or an `empty_tuple`
