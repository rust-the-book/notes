# Variables and Mutability

```{.rust}
fn main() {
    let x = 5;
    println!("The value of x is: {}", x);
    x = 6;
    println!("The value of x is: {}", x);
}
```

- *immutable* by default
- above code, we get `cannot assign twice to immutable variable` 
- one part of code thinks `x` won't change, another part of code changes it, rust prevents this 
- `let mut x = 5`, to make it mutable

## Difference between variables & constants

- *variables* `immutable` by *default*, but can be `mutable`
    - declare as `let a = 10`
- *constants* always `immutable`, period point blank.
    - declared as `const MAGIC_PI: f32 = 3.14`
    - type is *explicit* and not *inferred* 
    - all *uppercase* & *underscore* separated  
    - valid throughout program, but *within the scope* declared in
    - can be declare global & local
    - value should be *constant expression* not return from *function calls*

## Shadowing

```{.rust}
let x = 5;
let x = x + 1;
let x = x + 2;
println!("x is {}", x);
```

- re-declaring a same variable
- differs from `mut` by allowing different types, while `mut` only allows same type
- `let mut x = 10; x = "a";` is not allowed
- `let x = 10; let x = "a";` is allowed
- allows name re-use, rather than *x_str* & *x_num*
