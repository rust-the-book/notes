# Control Flow

- run code or skip based on condition

## if expressions

```{.rust}
fn main() {
    let number = 5;

    if number > 5 {
        println!("bigger!");
    } else {
        println!("smaller!");
    }
}
```

- is an *expression*, so can be used in *statements*
- branch code based on condition
- starts with `if` keyword, followed by *condition*
- sometimes called *arms*, like `match`
- *condition* should be *boolean*, no auto coersion 
- evaluates & returns the last *expression* of matching arm

## Handling multiple conditions with else if

```{.rust}
fn main() {
    let number = 6;

    if number % 4 == 0 {
        println!("divisible by 4");
    } else if number % 3 == 0 {
        println!("divisible by 3");
    } else if number % 2 == 0 {
        println!("divisible by 2");
    } else {
        println!("not divisible");
    }
}
```

- too much `else if` can clutter & look clumsy, use `match` instead

## Using if in a let statement

```{.rust}
let number = 10;
let number = if number > 5 {
    number + 1
} else {
    number + 2
};
```

- `if` is an *expression*, can be used in *statements*
- *numbers* are *expression* themselves
- above would assign `11` to number
- any *arm* could match, so final *expression* of each *arm* should be of *same type*
- following would causes *mismatched types* error
- rust needs *concrete type* at *compile* time
- won't handle hypothetical & complex logic, but be blunt with errors

```{.xml}
let number = if true {
    10
} else {
    "10"
}
```

## Repetition with loops

### Repeating code with loop

```{.rust}
fn main() {
    loop {
        println!("subscribe!");
    }
}
```

- `loop` repeats code forever, until explicit stop
- `break` to stop the loop or `panic` to stop program!

### Returning value from loop

```{.rust}
fn main() {
    let mut counter = 0;

    let result = loop {
        counter += 1; 

        if counter == 10 {
            break counter * 2; 
        }
    };

    println!("Result is {}", result);
}
```

- retry an operation that might fail
- pass result back after `break` expression
- above, `break countr * 2`, would return `20` when `counter` is `10`

### Conditional loops with while

```{.rust}
fn main() {
    let mut number = 0;

    while number < 3 {
        println!("number is {}", number);

        number += 1;
    }
}
```

- use a condition to auto break out of loop when it fails
- can be done with `loop`, `if else` and `break`, but rust has `while` & `for`

### Looping through a collection with for

```{.rust}
fn main() {
    let array = [1, 2, 3, 4, 5];

    let mut index = 0;

    while index < 5 {
        println!("value is {}", array[index]);

        index += 1;
    }

    for value in array.iter() {
        println!("value is {}", value);
    }

    for i in (1..6).rev() {
        println!("lift off in {}", i);
    }
}
```

- loop over collections like `array`
- can be done with `while` like above
    - error prone (human) with bad increment & wrong conditions
    - slow, compiler adds runtime code for conditional check
- better alternative `for`
- fixed issues - neither reducing nor crossing bounds of array
- *most used* loop in rust
- countdown examples would be implemented with `Range` provided by std lib
