# Data Types

- every *value* has *type*
- rust is *statically* typed
- has *scalar* & *compound* types
- `let x = 5`, rust can *infer* types during compile
- `let x = num.trim().parse().expect("Err!");`, compile error! rust can't guess which number type

## Scalar Types

- [integers](integer_types)
- boolean
- floating point
- characters

### Integer Types

- no fractional component
- i32 - *unsigned*
- u32 - *signed*, stored with *two's complement*
- 8bit - i8, u8
- 16bit - i16, u16
- 32bit - i32, u32
- 64bit - i64, u64
- arch - isize, usize depends on OS architecture
- literals
    - decimal - 3_14
    - hex - 0xff
    - octal - 0o77
    - bin - 0b1111_0000
    - byte (u8 only) - b'A'
- overflow
    - in *debug* mode, causes runtime panic!
    - in *release* mode, wrap around using *two's complement*. `u8` 0 to 255, `256` becomes `0`, `257` becomes `1`

### Floating Point

 - only `f32` & `f64` variants
 - `f64` is *default* in modern cpu's, as fast as `f32` and more *precision*
 - `f32` - single precision
 - `f64` - double precision
 - IEEE 754 standard representation

### Boolean

- `true` and `false`

### Character type

```{.rust}
let c = 'Z';
let c = 'ℤ'
let c = '😻'
```

- declared  with `char` keyword
- enclosed in single quotes `''`
- 4 bytes
- unicode scalar value (supports more than ASCII)
- U+0000 to U+D7FF and U+E000 to U+10FFFF
- *character* different from human thought & explained later

## Compound Types

- group multiple values
- `tuples` & `arrays`

### Tuples

```{.rust}
let tuple: (String, i32, f32) = (String::from("rust"), 10, 10.0);
let (x, y, z) = tuple;
```

- group different value
- can't *grow* or *shrink* once declared
- `let (x, y, z) = tuple;` - *pattern matching* to *destructure* a tuple
- access element by index - `tuple.0`, `tuple.1`

### Arrays

```{.xml}
let array = [1, 2, 3, 4, 5];
let array: [i32; 5] = [1, 2, 3, 4, 5];
let array = [10; 5];
```

- value of same type
- fixed length
- allocated on *stack*
- specify type like `[type; size]`
- specify initialization like `[initial_value; size]`

#### Access array elements

``` {.rust}
let array = [1, 2, 3];
```

- use index `a[0]`, `a[1]`
- index greater than array size panics with `index out of bounds`
- other languages, invalid memory can be accessed & rust prevents that
