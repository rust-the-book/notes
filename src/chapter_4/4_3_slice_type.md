# Slice type

```{.rust}
fn main() {
    let mut s = String::from("hello world");

    let word = first_word(&s);

    s.clear()
}

fn first_word(s: &String) -> usize {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return i;
        }
    }

    s.len()
}
```

- data type that doesn't have `ownership`
- `as_bytes` returns `byte` array of `String`
- `iter` returns interator
- `enumerate` returns `tuple(index, &item)`
- `(i, &item)` use `pattern` to *destructure* 
- `b' '` is `byte literal`
- works fine, but we return *index* of 1st word
- what happens if we clear `s` and try to access invalid location with index?

## String slice

```{.rust}
let s = String::from("hello world");

let s1 = &s[0..5];
let s2 = &s[6..11];
```

- `reference` to part of `String`
- similar to `&s`, but we take *part of it*
- stores a pointer to *starting index* & *length* which is *ending* minus *starting* index

```{.rust}
let s = String::from("hello world");
let from_start = &s[..11];
let till_end = &s[0..];
let full = &s[..]
```

- ignore start index to slice from `0`
- ignore end index to slice till length of string
- ignore both to slice entire string
- slices should occur at valid UTF-8 boundaries!

```{.rust}
fn main() {
    let mut s = String::from("hello world");

    let word = first_word(&s);

    s.clear()

    println!("word is {}", word);
}

fn first_word(s: &String) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[..i];
        }
    }

    &s[..]
}
```

- `s.clear()` tries to *mutably borrow* `s`
- we also have `string slice` called `word` & used after `s.clear()`
- remember? can't have `mutable` & `immutable` reference at same time

## String literals are slices

```{.rust}
let s = "hello world";
```

- `s` is a `literal`, `immutable`, hardoded & *points* to specific point in executable
- see! this is very much similar to `string slices` - `reference`, `immutable`, `points to memory`

## String slice as parameters

```{.rust}
fn main() {
    let s = String::from("hello world");
    let word = first_word(&s);
    let word = first_word(&s[..]);
    let word = first_word(&s[0..s.len()]);

    let s = "hello world";
    let word = first_word(&s);
    let word = first_word(&s[..]);
    let word = first_word(&s[0..s.len()]);
}

fn first_word(s: &str) -> &str {}
```

- all the above are valid
- because they are all `reference`, `immutable` & `points to memory`

## Other slices

```{.rust}
let array = [1, 2, 3, 4, 5];

let a1 = &array[..];
let a2 = &array[0..];
let a3 = &array[..array.len()];
```

- all the above are valid
- type is `&[i32]`
- like `&str` stores `pointer` & `length` (diff of *start* & *end* index)
