# What is Ownership?

- central feature
- straight forward to explain
- deep implications at language level
- some langs use *garbage collector*
- some langs rely on manual *alloc* & *dealloc*

## Stack and heap

- `stack` last in first out
    - data stored should have known *fixed size*
    - else they go to *heap*
    - fast
    - no allocation
    - free space always on top
    - values popped off when function exits
- `heap`
    - less organized
    - cpu finds empty spot for data, allocates & returns a *pointer*
    - slow to allocate & find data
    - need to be properly managed 

## Ownership rules

- each value has a variable called `owner`
- only `one owner` for a `value` at a time
- when owner goes *out of scope*, value is dropped

## Variable scope

```{.rust}
fn main() {
    let s = "hello";

    println!("{}, world!", s);
}
```

- `scope` - *range* in a program a value is *valid*
- `s` - valid within `{` to `}`
- `string literals` hardcoded into the program & immutable

## String type

- stored on `heap`
- `String::from("hello")` - create `String` from `string literals`
- mutable

## Memory & Allocation

- `string literals` hardcoded into executable
- `String` - to make it mutable & growable
    - can't know size at compile time
    - at runtime, allocated by OS
    - need a way to release the memory
- other langs use `GC`
- others langs do it manually, but proven to be difficult
- can't deallocate too early
- can't deallocate twice
- no deallocation means wasted memory
- need exactly one pair allocate & deallocate
- above, `String::from("hello")` asks OS to allocate
- rust deallocates, when owner goes out of scope, here `s`
- similar to *Resource Acquisition Is Initialization (RAII)* in `C++`

## Ways variables & data interact: move

```{.rust}
let x = 5;
let y = x;
```

- multiple variables interact with same data
- `x` & `y` both have `5` & stored on `stack`

```{.rust}
let s1 = String::from("hello");
let s2 = s1;
println!("s1 {}", s1); // gives error!
```

- `s1` stores a `pointer`
    - location in heap
    - length
    - capacity
- if `s2` copies this pointer, when `s1` & `s2` goes out of scope, rust would try `double free` the same memory
- nasty memory corruption issues
- instead, rust invalidates `s1` and `move` the `pointer` into `s1`
- can't use `s1` after `s2 = s1` statement

## Ways variables & data interact: clone

```{.rust}
let s1 = String::from("hello");
let s2 = s1.clone();
println!("s1 {}, s2 {}", s1, s2);
```

- to *deepcopy* data we `clone`
- *heap* data copied

## Stack only data: copy

```{.rust}
let x = 5;
let y = x;
println!("x {}, y {}", x, y);
```

- why does the above work? no `clone` here!
- `primitives` like `integer`, size known at *compile* time
- can be easily copied, since its in `stack`
- `Copy trait` makes this happen
- a type can't have both `Copy` & `Drop` traits implemented
- which types have `Copy`?
    - all integers
    - boolean
    - floating point
    - character
    - tuples, only if they contain `Copy` types `let t = (10, 'c', 10.0, true)`

## Ownership and functions

```{.rust}
fn main() {
    let s = String::from("hello");  // s comes into scope

    takes_ownership(s);             // s's value moves into the function...
                                    // ... and so is no longer valid here

    let x = 5;                      // x comes into scope

    makes_copy(x);                  // x would move into the function,
                                    // but i32 is Copy, so it’s okay to still
                                    // use x afterward

} // Here, x goes out of scope, then s. But because s's value was moved, nothing
  // special happens.

fn takes_ownership(some_string: String) { // some_string comes into scope
    println!("{}", some_string);
} // Here, some_string goes out of scope and `drop` is called. The backing
  // memory is freed.

fn makes_copy(some_integer: i32) { // some_integer comes into scope
    println!("{}", some_integer);
} // Here, some_integer goes out of scope. Nothing special happens.

```

- passing value to function, similar to `s2 = s1`
- in above, `s` *moved* into `some_string`, and invalidated
- in above, `x` *copied* to `some_integer`, and valid

## Return values and scope

```{.rust}
fn main() {
    let s1 = gives_ownership();         // gives_ownership moves its return
                                        // value into s1

    let s2 = String::from("hello");     // s2 comes into scope

    let s3 = takes_and_gives_back(s2);  // s2 is moved into
                                        // takes_and_gives_back, which also
                                        // moves its return value into s3
} // Here, s3 goes out of scope and is dropped. s2 goes out of scope but was
  // moved, so nothing happens. s1 goes out of scope and is dropped.

fn gives_ownership() -> String {             // gives_ownership will move its
                                             // return value into the function
                                             // that calls it

    let some_string = String::from("hello"); // some_string comes into scope

    some_string                              // some_string is returned and
                                             // moves out to the calling
                                             // function
}

// takes_and_gives_back will take a String and return one
fn takes_and_gives_back(a_string: String) -> String { // a_string comes into
                                                      // scope

    a_string  // a_string is returned and moves out to the calling function
}
```

- ownership can be transferred by returning value
- this process of *giving* & *taking* back ownership *tedious*
- so, rust has `references`
