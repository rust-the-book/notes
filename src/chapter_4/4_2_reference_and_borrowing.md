# Reference and Borrowing

```{.rust}
fn main() {
    let s1 = String::from("hello");

    let (s2, len) = calculate_length(s1);

    println!("len is {}", len);
}

fn calculate_length(s: String) -> (String, usize) {
    let length = s.len();

    (s, length)
}
```

- we need to calculate length & return ownership back to `main`
- code works, but cumbersome, can get out of hand quickly

```{.rust}
fn main() {
    let s1 = String::from("hello");

    let len = calculate_length(&s1);

    println!("len is {}", len);
}

fn calculate_length(s: &String) -> usize {
    s.len() 
}
```

- clutter code removed
- `&` means `reference`
- we `borrow` instead of taking `ownership` of `s1`
- `*` means `dereference`
- `s` borrows `s1`, so even if `s` goes out of scope, nothing happens to value
- remember? only when `owner` goes out of scope, `value` is *dropped*
- here `s1` is still `owner`

```{.rust}
fn main() {
    let s = String::from("hello");

    change(&s);
}

fn change(s: &String) {
    s.push_str(", world!");
}
```

- this doesn't work
- like `variables`, `references` are *immutable* by default

## Mutable reference

```{.rust}
fn main() {
    let mut s = String::from("hello");

    change(&mut s);
}

fn change(s: &mut String) {
    s.push_str(", world!");
}
```
- `let mut s` says variable is `mutable` in the 1st place
- `&mut s` says its a `mutable reference`
- `s: &mut String` says its a `mutable borrow`

```{.rust}
let mut s = String::from("hello");

let r1 = &mut s;
let r2 = &mut s;
```
- BIG NO! only one `mutable reference` at a time!
- this prevents *data race* 
    - multiple mutable pointers change data in unpredicatable way
    - one pointer writes
    - one pointer reads
    - how to *synchronize* this?

```{.rust}
let mut s = String::from("hello");

{
    let r1 = &mut s;
}

let r2 = &mut s;
```

- use `{}` to create *new scope*

```{.rust}
let mut s = String::from("hello");

let r1 = &s;
let r2 = &s;
let r3 = &mut s;
```

- BIG NO! can't have `mutable` & `immutable` reference in same scope, because `r1` & `r2` expect data not change, but changed by `r3`
- can have multiple `immutable` references, as they can't change data

```{.rust}
fn main() {
    let mut s = String::from("hello");

    let r1 = &s;
    let r2 = &s;

    println!("r1 {}, r2 {}", r1, r2);

    let r3 = &mut s;

    r3.push_str(", world!");

    println!("r3 {}", r3);
}
```

- valid? rust devs incorcorpated this in new version
- `r1` & `r2` not used after `r3` declaration
- so `r3` is valid and safe to use

## Dangling references

```{.rust}
fn main() {
    let points_to_nothing = dangling_reference();
}

fn dangling_reference() -> &String { // returns a reference to a String
    let s = String::from("hello"); // s created here!

    &s // we return a reference to the String, s
} // Here, s goes out of scope, and is dropped. Its memory goes away.
  // Danger!
```

- `pointer` to value in memory exists, but `value` cleared
- `s` goes out of scope, when `dangling_reference` returns
- `&s` points to nothing!
- but, rust throws `missing lifetime parameter` error

## Rules of reference

- only one `mutable` reference at a time
- can't have `mutable` & `immutable` references same time
- can have multiple `immutable` references
- references should always be valid
