# Traits - Defining Shared Behavior

* *functionality* a `type` has & can share with others
* `trait bounds` - specify generic types if they have certain behavior
* similar to `interfaces`, but with some difference, ofcourse!

## Defining a trait

```{.rust}
pub trait Summary {
    fn summarize(&self) -> String;
}

```

* *behavior* - `methods` we can call on a `type`
* different types, share same behavior - if same `method` available on those types
* a way to *group* method signatures, to define behavior, for specific purpose

## Implement a trait on type

```{.rust}
struct NewsArticle {
    headline: String,
    location: String,
    author: String,
    content: String
}

impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!("{}, by {} {}", self.headline, self.author, self.location) 
    }
}

struct Tweet {
    username: String,
    content: String,
    reply: String,
    retweet: bool
}

impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("{}: {}", self.username, self.content) 
    }
}

fn main() {
    let news = NewsArticle {
        headline: String::from("Covid-19 is wreaking havoc across the globle"),
        location: String::from("Everywhere"),
        author: String::from("Rust Bot"),
        content: String::from("Covid-19 is wreaking havoc across the globle"),
    };
    println!("news: {}", news.summarize());

    let news = Tweet {
        username: String::from("Rust Bot"),
        content: String::from("Covid-19 is wreaking havoc across the globle"),
        reply: String::from("Indeed!"),
        retweet: true
    };
    println!("tweet: {}", tweet.summarize());
}
```

* *trait* implementation similar to *methods*
* syntax `impl <trait> for <type>`
* **restriction** - we can implement `trait` on a `type` if,
    * `trait` or `type` is *local* to *our crate*
    * `Display` trait can be implemented on `Tweet` or,
    * `Summary` trait can be implemented on `Vec<T>`
    * but, `Display` trait can't be implemented on `Vec<T>`
    * because both `Display` trait & `Vec<T>` type are *external* to *our crate*
    * this is called `coherence`, more specifically `orphan rule` - makes sure other's code won't break ours

## Default Implementations

```{.rust}
trait Summary {
    fn summarize_author(&self) -> String;
    
    fn summarize(&self) -> String {
        format!("(default summary for author {}...)", self.summarize_author())
    }
}

impl Summary for Tweet {
    fn summarize_author(&self) -> String {
        format!("@{}", self.username) 
    }
}
```

* can have both *method signature* (`summarize_author`) & *default implementation* (`summarize`)
* can call other `methods`, even if there is no *default implemenation*
* `overriding` a *default implementation*, same as *providing implementation*, no special keyword!

## Trait as parameters

```{.rust}
fn notify(item: impl Summary) {
    println!("Breaking news! {}", item.summarize());
}
```

* create function that accepts different types
* `notify` accepts any `item`, as long as `type` implements `Summary` trait

## Trait bound syntax

```{.rust}
fn notify<T: Summary>(item: T) {
    println!("Breaking news! {}", item.summarize());
}
```

* `impl Trait` is a *syntactic sugar* for `trait bound` syntax
* more *verbose*
* `impl Trait` syntax used in *simple use cases*

```{.rust}
fn notify(item1: impl Summary, item2: impl Summary) {}

fn main() {
    notify(news, tweet);
}
```

* `impl Trait` syntax allows `item1` & `item2` be passed different types, as long as they implement `Summary` trait

```{.rust}
fn notify<T: Summary>(item1: T, item2: T) {}

fn main() {
    notify(news1, news2);
}
```

* `trait bound` syntax forces both `item1` & `item2` be passed same type

## Specifying multiple trait bounds with + syntax

```{.rust}
fn notify(item: impl Display + Summary) {}

fn notify<T: Display + Summary>(item: T) {}
```

* `+` says `item` should implement both `Display` & `Summary` traits
* same for `impl Trait` & `trait bound` syntax

## Clearer trait bound with where syntax

```{.rust}
fn notify<T: Display + Summary, U: Clone + Debug>(item1: T, item2: U) -> i32 {}

fn notify<T, U>(item1: T, item2: U) -> i32
    where T: Display + Summary,
          U: Clone + Debug {}
```

* `trait bound` syntax makes function hard to read
* `where` syntax easier & just moves the trait part after the return type

## Returning types that implement traits

```{.rust}
fn returns_summarizable() -> impl Summary {
    Tweet {
        username: String::from("rustbot"),
        content: String::from("covid-19 is wreaking havoc!"),
        reply: false,
        retweet: false
    }
}

fn returns_summarizable(switch: bool) -> impl Summary {
    if switch {
        NewsArticle {
            headline: String::from("Covid-19 is wreaking havoc across the globle"),
            location: String::from("Everywhere"),
            author: String::from("Rust Bot"),
            content: String::from("Covid-19 is wreaking havoc across the globle"),
        }   
    }
    else{
        Tweet {
            username: String::from("rustbot"),
            content: String::from("covid-19 is wreaking havoc!"),
            reply: false,
            retweet: false
        }
    }
}
```

* any type can be returned that implements `Summary` trait
* `catch!` can return only one type, not either or, like above
* compiler must know which type would be returned for sure

## Fixing largest function with trait bounds

```{.rust}
fn largest<T: PartialOrd + Copy>(list: &[T]) -> T {
    let mut largest = list[0];

    for &item in list.iter() {
        if item > largest {
            largest = item; 
        } 
    }

    largest
}
```

* couldn't use `>` operator, coz it is defined as a default method in `std::cmp::PartialOrd` trait
* we need to say that `T` implements that trait
* `PartialOrd already available in prelude`
* `let mut largets = list[0]` gives error! since `T` is generic, there are types that may or may not implement `Copy` trait, so this slice is assumed to contain *non-copiable* elements
* we include `Copy` to `trait bound` syntax

## Using trait bounds to conditionally implement methods

```{.rust}
use std::fmt::Displayl

struct Pair<T> {
    x: T,
    y: T
}

impl<T> Pair<T> {
    fn new(x: T, y: T) -> Self {
        Self {
            x,
            y
        } 
    }
}

impl<T: Display + PartialOrd> Pair<T> {
    fn cmp_display(&self) {
        if self.x > self.y {
            println!("The largest member is x = {}", self.x); 
        }
        else {
            println!("The largest member is y = {}", self.y)
        }
    }
}
```

* implement `new` for all `T`
* implement `cmp_display` for all `T`, if and only if `T` implements `Display` & `PartialOrd` 

```{.rust}
impl<T: Display> ToString for T {
    // sts
}
```

* implement trait for any type that implements a certain trait
* `Implementations of a trait on any type that satisfies the trait bounds are called "blanket implementations"`
* *blanket implementation* seen all of std lib
* above is from std lib, `ToString` is implemented for all `T` that implements `Display`
