# Validate references with lifetimes

* every `reference` has a *lifetime*
* its `scope` within with `reference` is *valid*

## Preventing dangling references with lifetimes

```{.rust}
fn main() {
    let r;

    {
        let x = 5;
        r = &x;
    }

    println!("r is {}", r);
}
```

* main aim - prevent *dangling reference*
* `x` doesn't live long enough, so `r` can't *borrow* it

## Borrow checker

```{.rust}
{
    let r;                // ---------+-- 'a
                          //          |
    {                     //          |
        let x = 5;        // -+-- 'b  |
        r = &x;           //  |       |
    }                     // -+       |
                          //          |
    println!("r: {}", r); //          |
}                         // ---------+
```

* `r` has *lifetime* `'a`
* `x` has *lifetime* `'b`
* `'a > 'b`, can't store `reference` to *memory* with smaller lifetime in bigger lifetime

```{.rust}

#![allow(unused_variables)]
fn main() {
    let r;                // ----------+-- 'a
                          //           |
    let x = 5             // --+-- 'b  |
	r = &x;               //   |       |
                          //   |       |
    println!("r: {}", r); //   |       |
                          // --+       |
}                         // ----------+
```

* `r` has *lifetime* `'a`
* `x` has *lifetime* `'b` 
* `'a > 'b`, but, both are valid until end of main, so this reference is valid!

## Generic lifetimes in functions

```{.rust}
fn longest(x: &str, y: &str) -> &str {
    if x.len() > y.len() {
        x 
    }
    else {
        y 
    }
}

fn main() {
    let string1 = String::from("abcd");
    let string2 = "xyz";

    let result = longest(string1.as_str(), string2);
    println!("longest string is {}", result);
}
```

* no idea of *concerete values* passed, so `if` or `else` might execute
* no idea of *concrete lifetimes* of values passed
* `borrow checker` has no idea how lifetimes of `x` & `y` relate to `return`

## Lifetime annotation syntax

```{.rust}
&i32 -> ref
&'a i32 -> ref with explicit lifetime
&'a mut i32' -> mutable ref with expicit lifetime
```

* `'a` is a `generic lifetime annotation`
* annotations doesn't change actual `reference's lifetime`
* just describes *relationship* of multiple references with eachother
* `function x(i1: &i32, i2: &i32)` - here, if `i1` has lifetime `'a` and `i2` has lifetime `'a`, then both `i1` & `i2` live as long as the generic lifetime `'a`

## Lifetime annotation in function signatures

```{.rust}
fn longest<'a>(x: &'a str, y: &'a) -> &'a str {
    if x.len() > y.len() {
        x 
    } else {
        y 
    }
}

fn main() {
    let s1 = String::from("abcd");

    {
        let s2 = String::from("xyz");
        let result = longest(s1.as_str(), s2);
        println!("longest is {}", result);
    }
}
```

* `'a` says that `x`, `y` & `return type` live *at least* as long as `'a`
* also, lifetime of `return type` is same as *smaller lifetime* of the passed in `references`
* concrete lifetime substituion, overlapping scope of `x` & `y`, `smallest` of the both is taken

```{.rust}
fn main() {
    let s1 = String::from("abcd");
    let result;
    {
        let s2 = String::from("xyz");
        result = longest(s1.as_str(), s2);
    }
    println!("longest is {}", result);
}
```
* `s1` lifetime *larger* than `s2`
* `result` has reference with lifetime of `s2`, so can't use `result` outside!

## Thinking in terms of lifetimes

```{.rust}
fn longest<'a>(x: &'a str, y: &str) -> &'a str {
    x
}
```

* here, `y` doesn't need `lifetime annotation`, because only `x` is returned always
* enough to specify lifetime of `x`

```{.rust}
fn longest<'a>(x: &str, y: &str ) -> &'a str {
    let result = String::from("longest");

    &result
}
```

* *error!* *error!* *error!*
* `lifetime` of `return type` should match `lifetime` of `one of the parameters`
* `lifetime all about connecting lifetime of inputs to outputs` - helps rust avoid `dangling references` & allow `memory safe operations`

## Lifetime annotations in struct definitions

```{.rust}
struct ImportantExcerpt<'a> {
    part: &'a str
}

fn main() {
    let novel = String::from("In the year 2020. A deadly pandemic brough the world to a standstill...");
    let first_sentence = novel.split('.')
        .next()
        .expect("Could not find '.'");
    let i = ImportantExcerpt { part: first_sentence };
}
```

* `'a` in struct means, `instance` can't outlive `reference` in `part` field
* both `novel` & `i` exists as long as `main` does

## Lifetime elision

```{.rust}
fn first_word(s: &str) -> &str {
    &s[..]
}
```

* *why did this work?*
* `pre 1.0`, rust required `lifetime annotations`
* since this is verbose, rust compiler included these patterns in `lifetime analysis` called `lifetime elision rules`
* lifetime on *function* or *method* input params called `input lifetimes`
* lifetime on *function* or *method* return types is called `output lifetimes`
* if `no explicit annotation`, rust uses *three* rules,
    * 1st rule - each `input reference parameter` gets its own lifetime annotation
    * 2nd rule - if only one `input reference parameter`, that lifetime is assigned to `output`
    * 3rd rule - if multiple `input lifetime parameters`, one of them is `&self` or `&mut self`, then its lifetime is assigned to `output`
* `1st rule` applies to `input parameters`, `2nd & 3rd` applies to `output parameters`

```{.rust}
fn first_word(s: &str) -> $str {}
```

* when `1st rule` is applied, we get, `fn first_word<'a> first_word(s: &'s str) -> &str {}`
* `2nd rule` is applied because its satisfied, we get, `fn first_word<'a> first_word(s: &'s str) -> &'a str {}`

```{.rust}
fn longest(x: &str, y: &str) -> &str {}
```

* when `1st rule` is applied, we get, `fn longest<'a, 'b>(x: &'a str, y: &'b str) -> &str {}`
* `2nd rule` can't be applied, since more than one input parameter
* `3rd` can't be applied either, since neither of the inputs are `&self` or `&mut self`
* lifetime of output parameter not found after 2nd & 3rd rule, so we get an error!

## Lifetime annotations in method definitions

```{.rust}
impl<'a> ImportantExcerpt<'a> {
    fn level(&self) -> i32 {
        0 
    }
}
```

* `lifetime parameters` - where we declare & use, depends on whether they're related to `struct fields` or `method params` & `return values`
* `method signatures` - reference might be tied to lifetime of reference in `struct fields` or *be independent*
* `1st rule` - `&self` gets own lifetime annotation
* output is not a reference
* `lifetime annotation required after impl keyword and type`

```{.rust}
impl<'a> ImportantExcerpt<'a> {
    fn announce_and_return_part(&self, announcement: &str) -> &str {
        println!("Announcement: {}", announcement);
        self.part
    }
}
```

* `1st rule` - `&self` & `announcement` gets own lifetime annotation
* `3rd rule` - lifetime of `&self` applied to `return type`

## Static lifetime

```{.rust}
let s: &'static str = "covid-19 is wreaking havoc";
```

* `special lifetime annotation`
* can live *throughout lifetime* of the program
* `string literals` have `static lifetime`
* make sure when you use `static lifetime`, you aren't using just because the compiler suggested it, you might be trying to having a `dangling reference`

## Generic type parameters, trait bounds & lifetimes together

```{.rust}
use std::fmt::Display;

fn longest_with_an_announcement<'a, T>(x: &'a str, y: &'a str, ann: T) -> &'a str
    where T: Display
{
    println!("Announcement: {}", ann);

    if x.len() > y.len() {
        x 
    } else {
        y 
    }
}
```

* `<'a, T>` - lifetime annotation are a type of `generic`, so they go in the same list as `generic type`
