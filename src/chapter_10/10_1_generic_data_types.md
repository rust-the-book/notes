# Generic Data Types

* functions, struct or enums - applicable

## In function definitions

```{.rust}
fn largest_i32(list: &[i32]) -> i32 {
    let mut largest = list[0];

    for &item in list {
        if item > largest {
            largest = item; 
        } 
    }

    largest
}

fn largest_char(list: &[char]) -> char {
    let mut largest = list[0];

    for &item in list {
        if item > largest {
            largest = item; 
        } 
    }

    largest
}

fn main() {
    let number_list = vec![0, 1, 1, 2, 3, 5, 8, 13, 21];
    println!("Largest number: {}", largest_i32(&number_list));

    let char_list = vec!['a', 'e', 'i', 'o', 'u'];
    println1("Largest char: {}", largest_char(&char_list));
}
```

* `largest_i32` & `largest_char` have same body, except the types

```{.rust}
fn largest<T>(list: &[T]) -> T {
    let mut largest = list[0];

    for &item in list {
        if item > largest {
            largest = item; 
        } 
    }

    largest
}

fn main() {
    let number_list = vec![0, 1, 1, 2, 3, 5, 8, 13, 21];
    println!("Largest number: {}", largest(&number_list));

    let char_list = vec!['a', 'e', 'i', 'o', 'u'];
    println1("Largest char: {}", largest(&char_list));
}
```

* Won't *compile*
* `if item > largest` causes error
* `T` doesn't implement `std::cmp::PartialOrd`
* `i32` & `char` might implement that, but `T` right now, represents all types, who may not implement this trait

## In struct definitions

```{.rust}
struct Point<T> {
    x: T,
    y: T
}

struct Point2<T, U> {
    x: T,
    y: U
}

fn main() {
    let p1 = Point { x: 5, y: 6 };
    let p2 = Point { x: 5.0, y: 6.0 };
    let p3 = Point { x: 5, y: 6.0 };
    let p4 = Point2 { x: 5, y: 6.0 };
}
```

* `p3` causes error, since `x` & `y` are of same generic type `T`
* `p4` has no issues, since `x` & `y` are of same generic types

## In enum definitions

```{.rust}
enum Options<T> {
    Some(T),
    None
}

enum Result<T, E> {
    Ok(T),
    Err(E)
}
```

## In method definitions

```{.rust}
impl<T> Point<T> {
    fn x(&self) -> &T {
        &self.x 
    }
}

impl Point<f32> {
    fn distance_from_origin(&self) -> f32 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }
}
```

* generics applicable for `methods` implemented on `struct` & `enum`
* why `T` in `impl<T>`? to notify `rust` that `T` is a generic type rather than concrete type & method applicable for all types
* notice `impl Point<f32>`? here we explicitly implement a method only for `Point` of type `f32` & not `T`

```{.rust}
impl<T, U> Point<T, U> {
    fn mixup<V, W>(&self, other: &Point<V, W>) -> Point <T, W> {
        Point {
            x: self.x,
            y: other.y
        } 
    }
}

fn main() {
    let p1 = Point { x: 5, y: 6.0 };
    let p2 = Point { x: "Hello", y: 'c' };
    let p3 = p1.mixup(&p2);
    println!("p3: {:#?}", p3);
}
```

* demonstrates `impl<T, U>` is at `struct` level & `fn mixup<V, W>` is only at `method` level

## Performance of code using generics

* *no runtime cost* in using generics
* `rust` uses *monomorphization*, which turns generic code into concrete
* meaning, `let x = Point {x: 5, y: 6}`, generates `struct Point_i32 {x: i32, y: i32}` & `let x = Point_i32 {x: 5, y: 6}`
* and, `let x = Point {x: 5.0, y: 6.0}`, generates `struct Point_f64 {x: f64, y: f64}` & `let x = Point_f64 {x: 5.0, y: 6.0}`
