# Test organization

* `unit test` - testing pieces of code in isolation
* `integration test` - testing similar to calling a library externally (only pub members)

## Unit tests

* goes in `src` inside each file with the code to be tested

### Tests module and #[cfg(test)]

* `#[cfg(test)]` - rust compiles this only for `cargo test`, not during `cargo build`
* `integration test` goes in its own directory
* `cfg` means `configuration`, tells Rust should include following component for certain config options, `test` in this case

### Testing private functions

```{.rust}
pub fn add_two(a: i32) -> i32 {
    internal_adder(a, 2)
}

fn internal_adder(a: i32, b: i32) -> i32 {
    a + b
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn internal_adder_works() {
        assert_eq!(internal_adder(1, 1), 2);
    }
}
```

* `mod tests` & `internal_adder` are under same module, so accessible

## Integration Tests

* similar to calling library externally
* can only access `Public APIs`
* makes sure library works together as expected
* individual working units failing integration means logic/design issue

### Tests directory

```{.rust}
use adder;

#[test]
fn it_adds_two() {
    assert_eq!(4, adder::add_two(2));
}
```

* `use adder` - each file in `tests` is a `separate crate`, need to bring library to scope
* no `cfg` needed, `tests` specially treated
* `cargo test --test integration_test` - run all tests in `tests/integration_test.rs`

### Submodules in integration tests

```{.rust}
tests
  ---- common
  -------- mod.rs
  ---- integration_test.rs


use adder;

mod common;

#[test]
fn it_adds_two() {
    common::setup();

    assert_eq!(4, adder::add_two(2));
}
```

* any sub-directory under `tests` not considered for integration tests
* any file under `tests` is tested, even if there are no `test` functions
* helps provide common functionality for other test files, like helpers
* create module `common` as above & use it to share functionality

### Integration tests for binary crates

* integration tests only work with library crates - `src/lib.rs`
* keep `src/main.rs` minimal, place all logic in `src/lib.rs`
* most rust projects with binary work this way!
