# Controlling how tests are run

* `cargo test` build code in test mode
* runs tests in parallel by default
* command opts - go to `cargo test` & `binary`
* `cargo test --help` - opts for `cargo test`
* `cargo test -- --help` - opts to `binary`

## Running tests in parallel or consecutive

* `cargo test -- --test-threads=1` - limits threads to 1
* useful when tests share something, like reading/writing to same file, can avoid test failures caused by file overwrites

## Showing function output

* only `stdout` from failed tests are printed
* use `cargo test -- --show-output` to print output of passing tests

## Running a subset of tests by name

```{.rust}
#[test]
fn add_one_hundred() {}

#[test]
fn add_one_thousand() {}

#[test]
fn subtract_one_hundred() {}
```

* use `cargo test <test_name>` to run that test, Ex: `cargo test add_one_hundred`
* here `<test_name>` can be a substring, any test having this name runs, Ex: `cargo test add_one`
* tests that didn't match are shown in `filtered` section of `test summary`
* useful when we want to run tests that concerns our code

## Ignoring some tests unless specifically requested

```{.rust}
#[test]
fn add_one_hundred() {}

#[test]
fn add_one_thousand() {}

#[test]
#[ignore]
fn subtract_one_googol() {}

#[test]
#[ignore]
fn one_googol_factorial() {}
```

* useful when suite contains long & expensive tests
* tests ignored are shown in `ignored` section of `test summary`
* use `cargo test -- --ignored`, to run ignored tests
