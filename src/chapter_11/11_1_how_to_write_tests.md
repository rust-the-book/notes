# How to write tests

* *functions* that verify code is working as expected
* typical parts,
    * setup state
    * run code to be tested
    * assert results

## Anatomy of test function

* any *function* `annotated` with `test` attribute - `#[test]`
* creating library projects with cargo, already have a test module to get started

``` {.rust}
cargo new adder --lib

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

cargo test

running 1 test
test tests::it_works ... ok

test result: ok. 1 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out

   Doc-tests adder

running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out
```

* test result shows various stats, `measured` is available in `nightly build`
* `Doc-tests` shows any document related tests

```{.rust}
running 2 tests
test tests::it_works ... ok
test tests::it_fails ... FAILED

failures:

---- tests::it_fails stdout ----
thread 'tests::it_fails' panicked at 'I failed!', src/lib.rs:10:9
note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace


failures:
    tests::it_fails

test result: FAILED. 1 passed; 1 failed; 0 ignored; 0 measured; 0 filtered out

error: test failed, to rerun pass '--lib'
```

* `tests::it_fails stdout` shows details about why a test failed

## Checking results with assert! macro

```{.rust}
#[deive(Debug)]
struct Rectangle {
    width: u32,
    height: u32
}

impl Rectangle {
    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height 
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn larger_rectangle_can_hold_smaller() {
        let larger = Rectangle { width: 100, height: 90};
        let smaller = Rectangle { width: 50, height: 50};

        assert!(larger.can_holder(&smaller));
    }

    #[test]
    fn smaller_rectangle_can_hold_larger() {
        let larger = Rectangle { width: 100, height: 90};
        let smaller = Rectangle { width: 50, height: 50};

        assert!(!smaller.can_hold(&larger));
    }
}
```

* `use super::*` - use components (like `Rectangle`) outside of tests

## Testing equality with assert_eq! and assert_ne! micros

```{.rust}
pub fn add_two(a: i32) -> i32 {
    a + 2
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_adds_two() {
        assert_eq!(add_two(2), 4);
    }
}
```

* rust tests doesn't have concept of `expected` & `actual` result, just `left` & `right` didn't match
* `assert_eq!` & `assert_ne!` work using `==` & `!=`
* values need to implement,
    * `PartialEq` - for comparison
    * `Debug` - to display value(using debug formatting) in case of failure 
* `struct` & `enum` can derive trait `#[derive(PartialEq, Debug)]`

## Adding custom failure message

```{.rust}
fn greeting(name: &str) -> String {
    format!("Hello {}", name)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn greeting_contains_name() {
        let result = greeting("Carol");
        assert!(result.contains("Carol"), "Greeting didn't have name, result was: {}", result);
    }
}
```

* some failures require useful message, like above
* in `greeting` function, its possible that message is changed in version, so we can't use `assert_eq!` for all possible values. Instead, we check if it contains the name we passed
* `assert!` doesn't display values, so we provide a custom failure message, saying what the actual result was
* Syntax,
    * `assert!(<test>, <format>, <format_args>)`
    * `assert_eq!(<left>, <right>, <format>, <format_args>)`

## Checking for panics with should_panic attribute

```{.rust}
struct Guess {
    value: i32
}

impl Guess {
    fn new(value: i32) -> Guess {
        if value < 1 || value > 100 {
            panic!("Guess value was lesser than 1, got {}", value);
        } 
        else {
            panic!("Guess value was greater than 100, got {}", value);
        }

        Guess {
            value 
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[should_panic]
    fn greater_than_100() {
        Guess::new(101);
    }

    #[test]
    #[should_panic]
    fn less_than_1() {
        Guess::new(0);
    }

    #[test]
    #[should_panic(expected="Guess value was greater than 100")]
    fn greater_than_100_expected() {
        Guess::new(101);
    }

    #[test]
    #[should_panic(expected="Guess value was lesser than 1")]
    fn lesser_than_1_expected() {
        Guess::new(0);
    }}
```

* Here, both tests would pass, since they `panic` as they should
* But, `should_panic` can be less accurate, since we know the code panicked, but not if its from our code or from something that our code called
* We can use `expected` arg as above, to check for a string in the panic message

## Using Result<T, E> in tests

```{.rust}
#[cfg(test)]
mod tests {
    #[test]
    fn it_works() -> Result<(), String> {
        if 2 + 2 == 4 {
            Ok(()) 
        }
        else {
            Err("2 + 2 != 4".to_string()) 
        }
    }

    #[test]
    fn ensure_file_present() -> Result<(), std::io::Error> {
        let f = std::fs::File::open("/etc/hosts")?;
        Ok(())
    }
}
```

* alternative for `assert_eq!`
* can use `?` operator on non-test code
* test fails if any calls returns `Err`
* if `Err` is returned from any of the above tests, test case fails!
