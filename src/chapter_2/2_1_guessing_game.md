# Programming a Guessing Game

```{.rust}
use std::io;

fn main() {
    println!("Guess the number!");

    println!("Please input your guess.");

    let mut guess = String::new();

    io::stdin().read_line(&mut guess)
        .expect("Failed to read line");

    println!("You guessed: {}", guess);
}
```

## Processing a Guess

- rust includes few types into scope during *prelude*

## Storing values in Variables

- variables - `immutable` by default
- `let a = 10` is immutable
- `let mut a = 10` is mutable
- `String` from std lib, growable, UTF-8 encoded
- `String::new()` - `::` says `new` is *associated function*, like *static* is `OOP`
- `io::stdin()` returns `std::io::Stdin`
- `new` a common pattern, indicates creates value for a type
- `&` - reference, immutable by default. Major rust feature, easy & safe usage of references.
- `&mut` - mutable reference

## Handling potential failure with Result

- `read_line()` return `std::io::Result`
- std lib has many `Result` types, `io::Result` one of 'em
- `Result` is *enum*, fixed set of values called *variants*
    - `Ok` - success & value
    - `Err`- failure & value
    - `expect` - if success returns value, else, panic & crash with given error message
- rust warns unused `Result` indicating error handling

## Printing value with println! placeholders

```{.rust}
println!("x is {}, y is {}", x, y)
```

- `{}` substituted by args passed to println!

## Using create to get more functionality

```{.toml}
Cargo.toml

[dependencies]
rand = "0.5.5"
```

- `crate` - collection of rust code files
- `guessing_game` - *binary* crate, executable!
- `rand` - *library* crate, to be used by other code
- *Cargo.toml* -> `[dependencies]` -> `<crate> = "<version>"`
- Semnatic versioning (SemVer) used. "0.5.5" shorthand for "^0.5.5" - any api compatible with this version

## Ensure reproducible builds with Cargo.lock file

- `rand 0.5.6` could have bugs & break code
- prevent using `Cargo.lock`
- generated during 1st build
- Versions of all deps loaded
- Next build, this is referred rather than re-check

## Update crate to new version

- `cargo update` ignore lock file & update deps
- only updates next latest update
- `rand 0.5.5` - only checks greater than `0.5.5` & less than `0.6.0`
- rand has `0.5.6` & `0.6.0`, updates to `0.5.6`
- modify `Cargo.toml` to get `0.6.0`, cargo re-calculates all versions & updates lock file

## Comparing guess to secret number

```{.rust}
use std::cmp::Ordering;

match guess.cmp(&secret_number) {
    Ordering::Less => println!("Too small!"),
    Ordering::Greater => println!("Too big!"),
    Ordering::Equal => println!("You win!"),
}
```

- `std::cmp::Ordering` - enum. variants - `Less`, `Greater` & `Equal`
- `match`
    - made of *arms*
    - *arm* has *pattern* & code to run if matched

```{.rust}
    let mut guess = String::new();
    let guess: u32 = guess.trim.parse().expect("Number!");
```

- `guess` re-declared twice! No, its a *shadow* in rust
- allows name re-use, rather than `guess_str`, `guess_val`
- `trim` removes *new line* (*Enter* pressed by user!)
- `parse` converts to a number, but we must explicit of number type i32, u32, f32, etc., ?
- input can have any character, so `parse` returns `Result`

## Allow multiple guess with loop

- `loop` creates infinite loop

## Quitting after a correct guess

- To stop use `break` anywhere within loop (should be reachable)

## Handling invalid input

- Use `continue` to skip further statements & continue the loop
