# Unrecoverable errors with panic!

on `panic!`,

- print failure
- unwind & cleanup `stack`

unwind stack or abort on panic?

- unwind - work!, clean up data, each function on stack
- small binary? `panic = 'abort'` in `[profile.release]` section in `Cargo.toml`
- *caveat* - memory clean? probably by `OS`

```{.rust}
fn main() {
    panic!("Crash and Burn!");
}
```

## Using panic backtrace

```{.rust}
let v = vec![1, 2, 3];

v[99];
```

- `panic!`, index out of bounds
- `C` returns something, bug! `buffer overread`, attacker exploit!
- `backtrace` - list of all functions called leading to `panic!`
- need `RUST_BACKTRACE=1` set & debug symbol enabled (default in `cargo run` && `cargo build`)
