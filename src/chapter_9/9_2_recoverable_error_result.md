# Recoverable errors with Result

```{.rust}
use std::fs::File;

fn main() {
    let f = File::open("hello.txt");
    let f = match f {
        Ok(file) => file,
        Err(err) => println!("Problem opening file: {:#?}", err)
    };
}
```

## Matching on different errors

```{.rust}
use std::fs::File;
use std::io::ErrorKind;

let f File::open("hello.txt");
let f = match f {
    Ok(file) => file,
    Err(err) = match err.Kind() {
        ErrorKind::NotFound => match File::create("hello.txt") => {
            Ok(file) => file,
            Err(err) => panic!("Unable to create file: {:#?}", err)
        },
        other_err => panic!("Something else occurred: {:#?}", other_err)
    }
};
```

- more matches, better but verbose!

```{.rust}
use std::fs::File;
use std::io::ErrorKind;

let f = File::open("hello.txt").unwrap_or_else(|error| {
    if error.kind() == ErrorKind.NotFound {
        File::Create("hello.txt").unwrap_or_else(|error| {
            panic!("Unable to create file: {:#?}", error);
        });
    } else {
        panic!("Something else occurred: {:#?}", error);
    }
});
```

- *rustacean* style!

## Shortcuts of panic on error - unwrap & expect

```{.rust}
let f = File::open("hello.txt").unwrap();
let f = File::open("hello.txt").expect("Unable to open file");
```

- `unwrap`, similar to `match` & `panic!`, default error message
- `expect`, similar to `unwrap`, takes a message to display

## Propagating errors

```{.rust}
fn read_username_from_file() -> Result<String, io::Error> {
    let mut f = File::open("username.txt") => {
        Ok(f) => f,
        Err(err) => return Err(err)
    };

    let mut s = String::new();

    match f.read_to_string(&mut s) {
        Ok(s) => Ok(s),
        Err(err) => Err(err)
    }
}
```

- good to let calling code handle errors

## Shortcut for propagating errors: ? operator

```{.rust}
fn read_username_from_file() -> Result<String, io::Error> {
    let mut f = File::open("username.txt")?;
    let mut s = String::new();
    f.read_to_string(&mut s)?;
    Ok(s)
}
```

- `?` similar to `match`
- return value if fine, else error
- error values with `?`, go through `from` function defined in `From` trait
- convert one error type to another
- for ex: error from `File::open` converted to `io::Error` defined in return type

```{.rust}
fn read_username_from_file() -> Result<String, io::Error> {
    let mut s = String::new();

    File::open("username.txt")?.read_to_string(&mut s)?;

    Ok(s)
}
```

- even shorter represenatation
- `?` operator eliminates boilerplate

```{.rust}
use std::fs;

fn read_username_from_file() -> Result<String, io::Error> {
    fs::read_to_string("username.txt")
}
```

- way shorter

## ? operator can be used in functions that return Result

```{.rust}
fn main() {
    let mut s = String::new();

    File::open("username.txt")?.read_to_string(&mut s)?;      
}
```

- this would throw an error
- `main` by default returns `()`
- `?` can be used in function that returns `Result` or `Options` or a type that implements `Try` trait
- to use `?`
    - try to change return type to `Result<T, E>`
    - or, use `match` or helper methods in `Result<T, E>` (`unwrap`, `expect`, `unwrap_or_else`) to handle it

```{.rust}
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let mut s = String::new();

    File::open("username.txt")?.read_to_string(&mut s)?;

    Ok(())
}
```

- `Box<dyn Error>` - `trait object`
- catch any kind of error!
