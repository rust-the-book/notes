# Hello World!

## Anatomy of a rust program

```{.rust}
    fn main() {
        println!("hello, world!");
    }  
```

- Every program starts with `main`
- `println!` is a `macro`
- Normal functions don't have `!` in them
