# Method Syntax

- *methods* similar to *functions*
- tied to an *instance* of `strcut`, `enum` or `trait object`
- `1st param` always `self` pointing to the *intance*

```{.rust}
impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height 
    }
}

let rectangle = Rectangle {width: 120, height: 110};

println!("area {}", rectangle.area());
```

- `impl` - add `functions` within `context` of `struct`
- `&self` - *mutable reference* to *instance* of `struct`

### Where's -> operator?

- `C` & `C++`
    - use `.` when calling `method` on an `object`
    - use `->` when calling `method` from `pointer to object`
        - Example: `p1` is `object`, then `p1.some_method()`
        - Example: `p2` is `pointer`, then `p1->some_method()` or `(*p2).some_method()`
        - `->` basically to `deference` a `pointer to object`
- `Rust`
    - has *automatic* `reference` & `dereference`
    - *method calls* one of few places this happens
    - if we do `obj.some_method()`, rust auto adds `&`, `*` or `&mut` to match *method signature*
    - Example: `p1.some_method(&p2)` or `(&p1).some_method(&p2)`
    - 1st one more clean
    - `some_method` clearly receives `&self`
    - so rust, based on *method name* & *receiver* finds `&self`, `&mut self` or `self`

## Method with more parameters

```{.rust} {
    impl Rectangle {
        fn can_hold(&self, other: &Rectangle) -> bool {
            self.area() > other.area() 
        }
    }
}
```

## Associated Functions

```{.rust}
impl Rectangle {
    fn square(size: u32) -> Rectangle {
        Rectangle {width: size, height: size} 
    }
}
```

- doesn't take `self` as paramter
- `associated` with a `struct`
- NOT A METHOD!
- like `String::from()`
- use `::` on `struct` to call, NOT ON INSTANCE!
- `::` used by both *associated functions* & `modules`

## Multiple impl blocks

```{.rust}
impl Rectangle {
    fn square(size: u32) -> Rectangle {
        Rectangle {width: size, height: size} 
    }
}

impl Rectangle {
    fn can_hold(&self, other: &Rectangle) -> bool {
        self.area() > other.area() 
    }
}
```

- perfectly valid!
