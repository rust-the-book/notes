# Defining & Instantiating Structs

```{.rust}
    struct User {
        username: String,
        email: String,
        sign_in_count: u64,
        active: bool
    }
```

- similar to `tuples`
- fields can be different type
- access fields by name rather than order, like in tuples

```{.rust}
    let mut user1 = User {
        username: String::from("rust"),
        email: String::from("rust@rust-lang.org"),
        sign_in_count: 1,
        active: true
    };

    user1.email = String::from("rustacean@rust-lang.org");
```

- `user1` is `instance` of struct `User`
- fields can be in any order
- if `mutable`, fields can be modified

## Field init shorthand

```{.rust}
    fn build_user(username: String, email: String) -> User {
        User {
            username,
            email,
            sign_in_count: 1,
            active: true
        } 
    }
```

- variable & field with same name? use field init shorthand

## Struct update syntax

```{.rust}
    let user2 = User {
        username: String::from("javascript"),
        email: String::from("javascript@js.org"),
        ..user1
    }
```

- re-use other fields from an instance in new instance? use field update syntax -> `..user1`

## Tuple structs

```{.rust}
    struct Color(i32, i32, i32);
    struct Point(i32, i32, i32);

    let black = Color(0, 0, 0);
    let origin = Point(0, 0, 0);
    
    let Point(x, y, z) = origin;

    println!("r {}, g {}, b {}", black.0, black.1, black.2);
```

- similar to tuples but gets a *name*
- `Color` & `Point` have same individual field types, but are different types entirely
- can be *destructured* using above *pattern*
- can be accessed using `.` notation follwed by *field index*

## Unit structs

```{.rust}
    struct Done{}
```

- No fields
- Useful implementing trait on type that doesn't store data
