# An example program using structs

```{.rust}
    fn main() {
        let width: u32 = 120;
        let height: u32 = 100;

        println!("Area: {}", area(width, height));
    }

    fn area(width: u32, height: u32) -> u32 {
        width * height 
    }
```

- program works fine
- params related, but not visibly expressed 

## Refactor with tuples

```{.rust}
    fn main() {
        let width: u32 = 120;
        let height: u32 = 100;

        let polygon = (width, height);

        println!("Area: {}", area(polygon));
    }

    fn area(polygon: (u32, u32)) -> u32 {
        polygon.0 * polygon.1 
    }
```

- fields grouped, looks better
- but, what if we do different calculation but send width & height swapped?

## Refactor with structs

```{.rust}
    struct Polygon {
        width: u32,
        height: u32
    }

    fn main() {
        let polygon = Polygon {
            width: 120,
            height: 100
        };

        println!("Area: {}", polygon);
    }

    fn area(polygon: Polygon) -> u32 {
        polygon.width * polygon.height 
    }
```

- now more order!

## Derived traits

```{.rust}
    println!("polygon is {}", polygon);
```

gives an error `doesn't implement std::fmt::Display`

```
= help: the trait `std::fmt::Display` is not implemented for `Polygon`
= note: in format strings you may be able to use `{:?}` (or {:#?} for pretty-print) instead
```

- `println!` handles many formatting
- `{}` denote `Display` formatting
- primitives are easy to display, as only one way to show them
- too many possibilities with `struct`

```{.rust}
    println!("polygon is {:?}", polygon);
```

gives an error `doesn't implement std::fmt::Debug`

```
= help: the trait `std::fmt::Debug` is not implemented for `Polygon`
= note: add `#[derive(Debug)]` or manually implement `std::fmt::Debug`
```

- add `#[derive(Debug)]`

```{.rust}
    #[derive(Debug)]
    struct Polygon
```

- `#{:?}` gives us `polygon is Polygon { width: 120, height: 100 }`
- `#{:#?}` gives us,

```
    polygon is Polygon {
        width: 120,
        height: 100
    }
```
