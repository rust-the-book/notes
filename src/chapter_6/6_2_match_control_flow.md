# match control flow operator

- is an `expression` itself
- like a *coin sorting machine*
- has multiple patterns called *arms*
- value matches an *arm*, it is executed

```{.rust}
enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter,
}

fn value_in_cents(coin: Coint) -> u8 {
    match coin {
        Coin::Penny => {
            println!("Lucky Penny!");
            1
        },
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter => 25,
    }
}

match <expression> {
    <pattern1> => <code>,
    <pattern2> => <code>,
    .
    .
    .
    <patternN> => <code>,
}
```

- `expression` unlike in `if`, can return any value
- `arm`
    - `pattern` - the value to match against the `expression`
    - `code`
        - to be executed when `arm` is matched
        - is an `expression`
        - result is returned as the result of entire `match expression`
        - multiple lines enclosed in `{}`

## Patterns that bind to values

```{.rust}
#[derive(Debug)]
enum UsState {
    Alabama,
    Alaska
}

enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState)
}

fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => {
            println!("Lucky Penny!");
            1
        },
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter(state) => {
            println!("Quarter belongs to state: {:#?}", state);
            25
        },
    }
}
```

- if `coin` matches the pattern `Coin::Quarter(state)`
- the value of `coin` is extracted into `state`
- its a `variant` of `UsState` enum

## Matching with Option<T>

```{.rust}
fn plus_one(value: Option<i32>) -> Option<i32> {
    match value {
        Some(val) => Some(val + 1),
        None => None
    }
}

let five = Some(5);
let six = plus_one(five);
let none = plus_one(None);
```

- `Some(5)` matches `Some(val)`
- `5` binds to `val` & returned plus one
- `None` matches `None` & its returned as is

## Matches are Exhaustive

```{.rust}
fn plus_one(value: Option<i32>) -> Option<i32> {
    match value {
        Some(val) => Some(val + 1),
    }
}
```

- we get an error above
- because we haven't tried to match all possible values of `value`
- here it could be `Some(val)` or `None` & we only matched `Some(val)`
- rust doesn't like to leave stuff hanging, which can lead to bugs
- matches are `exhaustive`

## _ placeholder

```{.rust}
let some_value = 10;

match some_value {
    1 => println!("1"),
    3 => println!("3"),
    5 => println!("5"),
    7 => println!("7"),
    _ => println!("{}", some_value),
}
```

- `_` is a *catchall* pattern
- sometimes we can't match all possible values, because there are too many or you don't care about the rest
