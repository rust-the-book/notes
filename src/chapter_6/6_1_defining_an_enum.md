# Defining an Enum

```{.rust}
enum IpAddrKind {
    V4,
    V6,
}
```

- `enum` - a type with `variants` that you can `enumerate`
- `value` can only be `one variant` at a time
- `127.0.0.1` is a `v4` not `v6`, but still an ip address!

## Enum Values

```{.rust}
fn main() {
    let v4 = IpAddrKind::V4;
    let v6 = IpAddrKind::V6;

    route(v4);
    route(v6);
}

fn route(ip_addr: IpAddrKind) {}
```

- pass `variants` of ip address to `route`

```{.rust}
struct IpAddr {
    kind: IpAddrKind,
    address: String
}

fn main() {
    let home = IpAddr {
        kind: IpAddrKind::V4,
        address: String::from("127.0.0.1")
    };

    let loopback = IpAddr {
        kind: IpAddrKind::V6,
        address: String::from("::1");
    };
}
```

- `struct IpAdd4` holds both ip address & its kind
- looks better now, but can be done better with `enum`

```{.rust}
enum IpAddr {
    V4(String),
    V6(String)
}

fn main() {
    let home = IpAddr::V4(String::from("127.0.0.1"));
    let loopback = IpAddr::V6(String::from("::1"));
}
```

- now, ip address & type are all in the same field

```{.rust}
enum IpAddr {
    V4(u8, u8, u8, u8),
    V6(String)
}

fn main() {
    let home = IpAddr::V4(127, 0, 0, 1));
    let loopback = IpAddr::V6(String::from("::1"));
}
```

- `variants` can hold different types - `string`, `primitives`, `structs` & `enum`! as well

```{.rust}
struct IpV4Addr {}

struct IpV6Addr {}

enum IpAddr {
    V4(IpV4Addr),
    V6(IpV6Addr)
}
```

- Standard lib also has an `IpAddr` enum for use
- above is an illustration of how its implemented

```{.rust}
enum Message {
    Quit,
    Move {x: i32, y: i32},
    Write(String),
    ChangeColor(i32, i32, i32)
}
```

- `Message` has multiple `variants`, each with different types!
    - `Quit` - holds no type
    - `Move` - holds `anonymous struct`!
    - `Write` - holds a `string`
    - `ChangeColor` - holds three numbers

```{.rust}
struct QuitMessage();

struct MoveMessage {
    x: i32,
    y: i32
}

Struct WriteMessage(String);

Struct ChangeColor(i32, i32, i32);
```

- above illustrates the same `enum variants` implemented as separate `structs`, not so good!
- these clearly points to a `message`, but physical grouping is lost
- have to pass all these to a function(sigh!)

```{.rust}
impl Message {
    fn call(&self) {}
}

let message = Message::Write(String::from("hello, world!"));
message.call();
```

- `enum` also can have `impl`!

## Options enum & its advantages over null

```{.rust}
enum Option<T> {
    Some(T),
    None
}

let some_string = Some("some");
let some_number = Some(1);

let null: Option<i32> = None;
```

- `null` a *billion dollar mistake!* - __Tony Hoare__
- most languages have it
- using `null reference` causes nasty issues
- `rust` doesn't have `null`
- `rust` instead provides `Option<T>` enum, with a `variant` that represent `null`
- already include in `prelude` phase!
- `Some` & `None` variants can be used without `Option::`
- when using `None` as value, `Option` type must be specified! like `let null: Option<i32> = None`. Because, compiler has no way to know, what type `null` would represent
- But, `Some(x)` means we have a value & `None` means no value, why do we need `Option<T>` at all?

```{.rust}
let x: i8 = 10;
let y: Option<i8> = Some(20);

let z = x + y;
```

- we get compiler error
- `Option<i8>` is entirely different from `i8`
- we just can't add it!
- moreover, when using `Option<T>`, we will be forced to check for null before using the value it stores
- unlike in other languages, if `y` was null, we would just go ahead and use it!
- by rust's design, when a value isn't `Option<T>`, then its safe to use it as is
- but, if its `Option<T>`, you better handle it!
- this is to prevent, `null` enormous reach in a code & its effects
