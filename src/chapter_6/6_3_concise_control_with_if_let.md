# Concise control flow with if let

```{.rust}
let some_value: u8 = Some(10);

match some_value {
    Some(3) => val,
    _ => ()
}
```

- when we are interested in only one value - Some(3)
- don't care about Some(1), Some(2) or None
- the above works good, but we can do it better & elegant!

```{.rust}
if let Some(3) = some_value {
    println!("three");
}

if let <pattern> = <expression> {
    // code
}

match <expression> {
    <pattern> => <code>
}
```

- same as `match`, but straight to point
- `match` & `if let` have an *inverse* of `pattern` & `expression` parts
- user to decide between `exhaustive` checking & `concise code`

```{.rust}
let mut non_quarter_coins = 0;

match coin {
    Coin::Quarter(state) => println!("Quarter belongs to state: {:#?}", state),
    _ => non_quarter_coins += 1
}

if let Coin::Quarter(state) = coin {
    println!("Quarter belongs to state: {:#?}", state);
} else {
    non_quarter_coins += 1;
}
```

- use `if let` when using `match` is too *verbose*
- above, both `match` & `if let` do the same, but `if let` feels more natural
